#include <msp430.h>                      // Generic MSP430 Device Include
#include "driverlib.h"                   // MSPWare Driver Library
#include "captivate.h"                   // CapTIvate Touch Software Library
#include "CAPT_App.h"                    // CapTIvate Application Code
#include "CAPT_BSP.h"                    // CapTIvate EVM Board Support Package
#include "setup.h"                       // Define e Variabili Globali
#include "Flash.h"                       // Flash Memory
#include "State_Machine.h"               // Macchia degli stati
#include <stdint.h>
/*
 * Prototipi funzioni.
 */
void init_adc1 (void);
void initI2C (void);

// PDO
void set_pdo(uint16_t volt, uint16_t amp, uint8_t pdo_number);
void change_pdo(uint8_t pdo_number);

// I2C
I2C_Mode I2C_Master_WriteReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t count);
I2C_Mode I2C_Master_ReadReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t count);

// Flash
void FRAMWrite (void);
void FRAMRead (void);

// Macchina degli stati
void initArray (void);
void init_Lamp_State_Machine (void);
void Get_Touch (void);
void Lamp_State_Machine_STEPS (void);
void RUN_State_Machine (void);
void Delay_With_Interrupt (uint32_t delay);
void FadeOn (void);
void FadeOff (void);
void Blink_Reset();
__interrupt void Timer1_A0_ISR(void);

// Varie
void CopyArray(uint8_t *source, uint8_t *dest, uint8_t count);
void Update_ADC_Value (void);
void delay_ms(unsigned int ms);
bool Battery_Low (void);
void Check_Negoziatore (void);

char buffer_rx[3] = {0};

void main(void)
{
    WDTCTL = WDTPW | WDTHOLD;
	BSP_configureMCU();

	initI2C();
	init_adc1();

	__delay_cycles(_50ms_Delay_C);
	//Check_Negoziatore();
	__delay_cycles(_50ms_Delay_C);
	/*
	if(!(ATTACHED))
	{
	    if (NEGOZIATORE == 15)
	    {
            LED2_ON;
	    	__delay_cycles(_50ms_Delay_C);
	        set_pdo(15,3,2);
            LED2_OFF;
	    	__delay_cycles(_50ms_Delay_C);
	        set_pdo(15,3,3);
            LED2_ON;
	    	__delay_cycles(_50ms_Delay_C);
            change_pdo(3);
            LED2_OFF;
	    }
	}
	 */
	P1DIR |= BIT1;                     // P1.1 output
	P1SEL1 |= BIT1;                    // P1.1 options select

	P2OUT |=  BIT5;                          	// Configure P2.5 as pulled-up
	P2REN |=  BIT5;                          	// P2.5 pull-up register enable
	if(ATTACHED) P2IES |=  BIT5;        // P2.5 Hi/Low edge
	else P2IES &= ~BIT5;                        // P2.5 Low/Hi edge
	P2IE  |=  BIT5;                           	// P2.5 interrupt enabled
	P2IFG &= ~BIT5;                         	// P2.5 IFG cleared

	// Disable the GPIO power-on default high-impedance mode to activate
	// previously configured port settings
	PM5CTL0 &= ~LOCKLPM5;

	TA0CCR0 = MAX_DUTY;         		            // PWM Period
	TA0CCTL1 = OUTMOD_7;                      	// CCR1 reset/set
	TA0CCR1 = 0;                            	// CCR1 PWM duty cycle
	TA0CTL = TASSEL__SMCLK | MC__UP | TACLR;  	// SMCLK, up mode, clear TAR

	// Timer1_A3 setup
	TA1CCTL0 = CCIE;                              // TACCR0 interrupt enabled
	TA1CCR0 = 33-1;
	TA1CTL = TASSEL_1 | MC_1;                     // ACLK, up mode

	__bis_SR_register(GIE);

	ADCCTL0 |= ADCENC | ADCSC;
    _delay_cycles(50000);

#ifdef NEGOZIATORE_ESTERNO
	while(ADCMEM0 < STARTUP_THREESHOLD)
    {
        ADCCTL0 |= ADCENC | ADCSC;
        LED2_ON;
        _delay_cycles(2000000);
        LED2_OFF;
        _delay_cycles(2000000);
     }
#else
	while(ADCMEM0 < STARTUP_THREESHOLD  && (ATTACHED))
    {
        ADCCTL0 |= ADCENC | ADCSC;
        LED2_ON;
        _delay_cycles(2000000);
        LED2_OFF;
        _delay_cycles(2000000);
     }
#endif

	FRAMRead();
	CAPT_appStart();
	init_Lamp_State_Machine();
	__delay_cycles(_50ms_Delay_C);
	//Check_Negoziatore();

	//MasterMode = I2C_Master_ReadReg(I2C_SLAVE_ADDR, 0x89, 4);

	while(1)
    {
		//Check_Negoziatore();
	    Update_ADC_Value();
        RUN_State_Machine();
	}
}
/*
 *  Macchina degli Stati
 */

void initArray (void)
{
    // 0,019x^2 + 0,00612x
    DimmLevel[0] = MIN_PWM;
    DimmLevel[1] = 45;
    DimmLevel[2] = 85;
    DimmLevel[3] = MAX_PWM;
}

void Set_Touch_Delay(uint32_t delay)
{
    mSecDisableMachineStateTouch = delay;
}

bool Touch_Enable()
{
    return Machine_State_Touch_Enable;
}

void Blink_Reset()
{
		if (!(ATTACHED))
		{
			if (Negoziatore_Attaccato == false)
			{
				Negoziatore_Attaccato = true;
				FadeOff();
				__delay_cycles(8000000);
				FadeOn();
			}
		}
		// Se lo stacco resetto variabile
		else
		{
			Negoziatore_Attaccato = false;
		}
}

void init_Lamp_State_Machine (void)
{
    initArray();
    DimmIndex = 0;
    Lamp_State = LAMP_OFF;
    DimmDirection = RISING;
    FRAMRead();
    Key_Last_Release = Key_Release_Counter - 1;

    // Primo Avvio
    if (DimmIndex == 0xFFFF)
        DimmIndex = 0;
    if (Lamp_State == 0xFFFF)
        Lamp_State = LAMP_OFF;
    if (DimmDirection == 0xFFFF)
        DimmDirection = RISING;

    if (Lamp_State == LAMP_ON)
    {
        FadeOn();
    }
    __no_operation();
}
void Get_Touch (void)
{
    Key_State = DISABLED;
    if (Touch_Enable() == true)
    {
        if (IS_TOUCHED)
        {
            if (Timer_State == false)
            {
                Key_Pressed_Counter ++;
            }

            Timer_State = true;

            LED2_ON;

            if ((mSecTouch > MINIMUM_SHORT_TOUCH_TIME) && (mSecTouch < MAXIMUM_SHORT_TOUCH_TIME))
            {
                Key_State = SHORT_PRESS;
            }
            else if (mSecTouch > MAXIMUM_SHORT_TOUCH_TIME)
            {
                Key_State = LONG_PRESS;
            }
        }
        else
        {
            if (Timer_State == true)
            {
                Key_Release_Counter ++;
            }
            LED2_OFF;
            Timer_State = false;
            mSecTouch = 0;
            Key_State = NOT_PRESSED;
        }
    }
    else
    {
        if (Timer_State == true)
        {
            Key_Release_Counter ++;
        }
        LED2_OFF;
        Timer_State = false;
        mSecTouch = 0;
        Key_State = DISABLED;
    }
}




void Auto_Delay (void)
{
    int i = MANUAL_RAMP_DELAY_TIME;
    while(--i > 1) __no_operation();
}
void Delay_With_Interrupt (uint32_t delay)
{
    int i = delay;
    while(--i > 1)
    	__no_operation();
}

void _Half_Sec_Clock_Timer (void)
{
    uint32_t i = HALF_SEC_RAMP_DELAY_TIME;
    while(--i > 1) __no_operation();
}

void delay_ms(unsigned int ms)
{
    while (ms)
    {
        __delay_cycles(16000);
        ms--;
    }
}

void Lamp_Sweep(int first_index, int second_index)
{
#ifdef SWEEP_ENABLE_DIMMERING
    int i = 0;
    // Primo Avvio
    if (first_index < 0)
    {
        int value_2 = DimmLevel[second_index];
        for (i = 0; i < value_2; i++)
        {
            Delay_With_Interrupt(SWEEP_TIME_DIMMERING);
            TIMER_PWM_OUT = i;
        }
    }

    // Prevenzione Errori
    int value_1 = DimmLevel[first_index];
    int value_2 = DimmLevel[second_index];

    // Rampa Salita
    if (first_index < second_index)
    {
        for (i = value_1; i < value_2; i++)
        {
            Delay_With_Interrupt(SWEEP_TIME_DIMMERING);
            TIMER_PWM_OUT = i;
        }

    }
    // Discesa
    else
    {
        for (i = value_1; i > value_2; i--)
        {
            Delay_With_Interrupt(SWEEP_TIME_DIMMERING);
            TIMER_PWM_OUT = i;
        }
    }
#endif
}
// Funzione non usata
bool Battery_Low (void)
{
    if ((ADC_Result < STARTUP_THREESHOLD) && (ATTACHED))
    {
        FadeOff();
        FRAMWrite();
        return true;
    }
    return false;
}

bool Exit_From_Battery_Low (void)
{
    if ((ADC_Result > (STARTUP_THREESHOLD + THREESHOLD_OUT_LOW_BATTERY)) || (!(ATTACHED)))
    {
        Array_Discharge_Timer_Index = 0;
        timer_discharge = 0;
        first_enter_low_battery = false;
        FadeOff();
        FRAMWrite();
        return true;
    }
    return false;
}


void Lamp_Set_PWM (void)
{
    #ifdef SWEEP_ENABLE_DIMMERING
    Lamp_Sweep(old_DimmIndex, DimmIndex);
    #else
    if (DimmIndex < 0)
    {
    	DimmIndex = 0;
    }
    TIMER_PWM_OUT = DimmLevel[DimmIndex];
    #endif
}


void Batteria_In_Scarica (void)
{
    if (BATTERIA_SI_STA_SCARICANDO)
    {
        first_enter_low_battery = true;
        if (Array_Discharge_Timer_Index == 0)
        {
            Lamp_Set_PWM();
        }
        if (Array_Discharge_Timer_Index == 15)
        {
			#ifdef AMBRINA
        		DimmIndex = 0;
			#endif
            TIMER_PWM_OUT = MIN_PWM;
            if (timer_discharge >= _15_Min)
            {
                TIMER_PWM_OUT = DUTY_OFF;
                Lamp_State = LAMP_NEED_CHARGE;
                FRAMWrite();
            }
            return;
        }
        if (timer_discharge >= Array_Discharge_Timer_Value[Array_Discharge_Timer_Index])
        {
            timer_discharge = 0;
            Array_Discharge_Timer_Index ++;
            FadeOff();
            __delay_cycles(16000000);
            FadeOn();
        }
    }
    else
    {
        Array_Discharge_Timer_Index = 0;
        timer_discharge = 0;
        Lamp_Set_PWM();
    }
}

void Blink_Full_Charge (void)
{
	/*
	bool attacched = false;
	if (ATTACHED)
	{
		attacched = true;
	}
	else
	{
		attacched = false;
	}*/

    //if ((BATTERIA_CARICA) && (attacched == false) && (Lamp_State == LAMP_FULL_CHARGE))
    if ((BATTERIA_CARICA) && (!(ATTACHED)) && (Lamp_State == LAMP_OFF))
    {
        lamp_is_full_of_charge = true;
        if (timer_charge >= _1_Min)
        {
            FadeOn();
            __delay_cycles(16000000);
            FadeOff();
            timer_charge = 0;
        }
    }
    else
    {
    	lamp_is_full_of_charge = false;
        timer_charge = 0;
    }
}
void Lamp_State_Machine_STEPS (void)
{
    switch (Lamp_State)
    {
        case LAMP_OFF:
        {
            Led_State = L_OFF;
            if (Key_State == LONG_PRESS)
            {
                Lamp_State = LAMP_LONG_PRESS;
            }
        	Blink_Full_Charge();

            break;
        }


        case LAMP_ON:
        {
            Led_State = L_ON;

            Batteria_In_Scarica();
    		Blink_Reset();

            switch (Key_State)
            {
                case SHORT_PRESS:
                {
                    Lamp_State = LAMP_SHORT_PRESS;
                    break;
                }

                case LONG_PRESS:
                {
                    Lamp_State = LAMP_LONG_PRESS;
                    break;
                }
            }
            break;
        }


        case LAMP_SHORT_PRESS:
        {
            switch (Key_State)
            {
                case NOT_PRESSED:
                {
                    Lamp_State = LAMP_DIMMERING;
                    break;
                }
                case LONG_PRESS:
                {
                    Lamp_State = LAMP_LONG_PRESS;
                    break;
                }
            }
            break;
        }


        case LAMP_LONG_PRESS:
        {
            switch (Led_State)
            {
                case L_ON:
                {
                    Led_State = L_OFF;
                    FadeOff();
                    break;
                }
                case L_OFF:
                {
                    Led_State = L_ON;
                    FadeOn();
                    break;
                }
            }
            Lamp_State = LAMP_WAIT_NOT_PRESS;
            break;
        }

        case LAMP_WAIT_NOT_PRESS:
        {
            if (Key_State == NOT_PRESSED)
            {
                switch (Led_State)
                {
                    case L_ON:
                    {
                        Lamp_State = LAMP_ON;
                        FRAMWrite();
                        break;
                    }
                    case L_OFF:
                    {
                        Lamp_State = LAMP_OFF;
                        FRAMWrite();
                        break;
                    }
                }
            }
            break;
        }

        case LAMP_DIMMERING:
        {
            #ifdef SWEEP_ENABLE_DIMMERING
            int old_DimmIndex = DimmIndex;
            #endif
            switch(DimmDirection)
            {
                case RISING:
                    if ((DimmIndex >= 0) && (DimmIndex <= STEP_PWM_ARRAY_SIZE - 1))
                    {
                        if (DimmIndex == STEP_PWM_ARRAY_SIZE - 1)
                        {
                            DimmDirection = FALLING;
                            DimmIndex--;
                            break;
                        }
                        DimmIndex ++;
                    }
                    break;
                case FALLING:
                    if ((DimmIndex >= 0) && (DimmIndex <= STEP_PWM_ARRAY_SIZE - 1))
                    {
                        if (DimmIndex == 0)
                        {
                            DimmDirection = RISING;
                            DimmIndex++;
                            break;
                        }
                        DimmIndex --;
                    }
                    break;
            }
            Lamp_Set_PWM();
            Lamp_State = LAMP_ON;
            FRAMWrite();
            break;
        }

        case LAMP_NEED_CHARGE:
        {
            _delay_cycles(2000000);
            _delay_cycles(2000000);
            _delay_cycles(2000000);
            ADCCTL0 |= ADCENC | ADCSC;
            LED2_ON;
            _delay_cycles(2000000);
            LED2_OFF;
            _delay_cycles(2000000);
            FRAMWrite();


            if (Exit_From_Battery_Low() == true)
            {
                //Lamp_State = LAMP_FULL_CHARGE;
            	Lamp_State = LAMP_OFF;
                FRAMWrite();
            }
            break;
        }

        case LAMP_FULL_CHARGE:
        	{
        		Led_State = L_OFF;
				Blink_Full_Charge();

				bool attacched = false;
				if (ATTACHED)
				{
					attacched = true;
				}
				else
				{
					attacched = false;
				}

				attacched = false;
				if (attacched == true)
				{
					Lamp_State = LAMP_OFF;
	                FRAMWrite();
				}

				if (Key_State == LONG_PRESS)
				{
					Lamp_State = LAMP_LONG_PRESS;
	                FRAMWrite();
				}
        	}
        	break;
    }
}
/*
void Lamp_State_Machine_LINEAR (void)
{
    switch (Lamp_State)
    {
        case LAMP_OFF:
        {
            Led_State = L_OFF;
            TIMER_PWM_OUT = 0;

            if (Battery_Low() == true)
            {
                Lamp_State = LAMP_NEED_CHARGE;
                FRAMWrite();
                break;
            }

            if (Key_State == SHORT_PRESS)
            {
                Lamp_State = LAMP_SHORT_PRESS;
            }
            break;
        }

        case LAMP_ON:
        {
            break;
        }

        case LAMP_SHORT_PRESS:
        {
            // Accensione e spegnimento
            switch (Led_State)
            {
                case L_OFF:
                {
                    Lamp_State = LAMP_ON;
                    break;
                }
                case L_ON:
                {
                    switch (Key_State)
                    {
                        case NOT_PRESSED:
                        {
                            Lamp_State = LAMP_OFF;
                            FRAMWrite();
                            break;
                        }
                        case LONG_PRESS:
                        {
                            Lamp_State = LAMP_LONG_PRESS;
                            FRAMWrite();
                            break;
                        }
                    }
                    break;
                }
            }
            break;
        }

        case LAMP_LONG_PRESS:
        {
            Lamp_State = LAMP_DIMMERING;
            break;
        }

        case LAMP_DIMMERING:
        {
            switch(DimmDirection)
            {
                case RISING:
                    if ((DimmIndex >= MIN_PWM) && (DimmIndex < MAX_PWM))
                    {
                        DimmIndex ++;
                        if (DimmIndex == MAX_PWM)
                        {
                            DimmDirection = FALLING;
                        }
                    }
                    break;

                case FALLING:
                    if ((DimmIndex > MIN_PWM) && (DimmIndex <= MAX_PWM))
                    {
                        DimmIndex --;
                        if (DimmIndex == MIN_PWM)
                        {
                            DimmDirection = RISING;
                        }
                    }
                    break;
            }
            TIMER_PWM_OUT = DimmIndex;
            Lamp_State = LAMP_ON;
            FRAMWrite();
            break;
        }

        case LAMP_NEED_CHARGE:
        {
            ADCCTL0 |= ADCENC | ADCSC;
            LED2_ON;
            _delay_cycles(2000000);
            LED2_OFF;
            _delay_cycles(2000000);

            if (Exit_From_Battery_Low() == true)
            {
                Lamp_State = LAMP_OFF;
                FRAMWrite();
            }
            break;
        }
    }
}
*/
void RUN_State_Machine (void)
{
    Get_Touch();
    STATE_MACHINE_FUNCTION
}
void FadeOn (void)
{
	if (DimmIndex < 0)
	{
		DimmIndex = 0;
	}
#ifdef AMBRINA
    #ifdef SWEEP_ENABLE_ON_OFF
        int i = 0;
        int max_level = DimmLevel[DimmIndex];

        if (Lamp_State == LAMP_OFF)
        {
        	max_level = MAX_DUTY;
        }


        for (i = 0; i < max_level; i++)
        {
            Delay_With_Interrupt(SWEEP_TIME_ON_OFF);
            TIMER_PWM_OUT = i;
        }
    #else
        if (Lamp_State == LAMP_OFF)
        {
        	TIMER_PWM_OUT = MAX_DUTY;
        }
        TIMER_PWM_OUT =  DimmLevel[DimmIndex];
    #endif
#endif
#ifdef FIAMMETTA
    #ifdef SWEEP_ENABLE_ON_OFF
        int i = 0;
        for (i = 0; i < DimmIndex; i++)
        {
            Delay_With_Interrupt(SWEEP_TIME_ON_OFF);
            TIMER_PWM_OUT = i;
        }
    #else
        TIMER_PWM_OUT = DimmIndex;
    #endif
#endif
}
void FadeOff (void)
{
	if (DimmIndex < 0)
	{
		DimmIndex = 0;
	}
#ifdef AMBRINA
    #ifdef SWEEP_ENABLE_ON_OFF
        int i = 0;
        int max_level = DimmLevel[DimmIndex];

        if (Lamp_State == LAMP_OFF)
        {
        	max_level = MAX_DUTY;
        }

        for (i = max_level; i >= 0; i--)
        {
            Delay_With_Interrupt(SWEEP_TIME_ON_OFF);
            TIMER_PWM_OUT = i;
        }
    #else
        TIMER_PWM_OUT =  0;
    #endif
#endif
#ifdef FIAMMETTA
    #ifdef SWEEP_ENABLE_ON_OFF
        int i = 0;
        for (i = DimmIndex; i >= 0; i--)
        {
            Delay_With_Interrupt(SWEEP_TIME_ON_OFF);
            TIMER_PWM_OUT = i;
        }
    #else
        TIMER_PWM_OUT =  0;
    #endif
#endif
}

void Update_ADC_Value (void)
{
    if (mSecADC >= _1_Sec) {
        ADCCTL0 |= ADCENC | ADCSC;
        mSecADC = 0;
    }
}

/*
 * Timer Touch Interrupt
 * Conta per quanti milli secondi � toccato il touch
 */
#pragma vector = TIMER1_A0_VECTOR
__interrupt void Timer1_A0_ISR(void)
{
    if(Timer_State == true)
    {
        if(mSecTouch < MAXIMUM_SHORT_TOUCH_TIME + 1)
        {
            mSecTouch ++;
        }
    }

    mSecADC++;

    if (BATTERIA_SI_STA_SCARICANDO && Lamp_State == LAMP_ON)
    {
        if (timer_discharge < _15_Min)
        {
            timer_discharge ++;
        }
    }

    if (stdTimer > 0)
    {
    	stdTimer --;
    }

    if (lamp_is_full_of_charge == true)
    {
        timer_charge ++;
    }

    if (time_elapse > 0)
    {
    	time_elapse --;
    }

    // Timer per il touch disabilitato
    if (mSecDisableMachineStateTouch > 0)
    {
        Machine_State_Touch_Enable = false;
        mSecDisableMachineStateTouch --;
        if (mSecDisableMachineStateTouch == 0)
        {
            Machine_State_Touch_Enable = true;
        }
    }
}


//******************************************************************************
// I2C Interrupt ***************************************************************
//******************************************************************************
#pragma vector = USCI_B0_VECTOR
__interrupt void USCI_B0_ISR(void)
{
	//Must read from UCB0RXBUF
	uint8_t rx_val = 0;
	switch(__even_in_range(UCB0IV, USCI_I2C_UCBIT9IFG))
	{
	case USCI_NONE:          break;         // Vector 0: No interrupts
	case USCI_I2C_UCALIFG:   break;         // Vector 2: ALIFG
	case USCI_I2C_UCNACKIFG:                // Vector 4: NACKIFG
		break;
	case USCI_I2C_UCSTTIFG:  break;         // Vector 6: STTIFG
	case USCI_I2C_UCSTPIFG:  break;         // Vector 8: STPIFG
	case USCI_I2C_UCRXIFG3:  break;         // Vector 10: RXIFG3
	case USCI_I2C_UCTXIFG3:  break;         // Vector 12: TXIFG3
	case USCI_I2C_UCRXIFG2:  break;         // Vector 14: RXIFG2
	case USCI_I2C_UCTXIFG2:  break;         // Vector 16: TXIFG2
	case USCI_I2C_UCRXIFG1:  break;         // Vector 18: RXIFG1
	case USCI_I2C_UCTXIFG1:  break;         // Vector 20: TXIFG1
	case USCI_I2C_UCRXIFG0:                 // Vector 22: RXIFG0
		rx_val = UCB0RXBUF;
		if (RXByteCtr)
		{
			ReceiveBuffer[ReceiveIndex++] = rx_val;
			RXByteCtr--;
		}

		if (RXByteCtr == 1)
		{
			UCB0CTLW0 |= UCTXSTP;
		}
		else if (RXByteCtr == 0)
		{
			UCB0IE &= ~UCRXIE;
			MasterMode = IDLE_MODE;
			__bic_SR_register_on_exit(CPUOFF);      // Exit LPM0
		}
		break;
	case USCI_I2C_UCTXIFG0:                 // Vector 24: TXIFG0
		switch (MasterMode)
		{
		case TX_REG_ADDRESS_MODE:
			UCB0TXBUF = TransmitRegAddr;
			if (RXByteCtr)
				MasterMode = SWITCH_TO_RX_MODE;   // Need to start receiving now
				else
					MasterMode = TX_DATA_MODE;        // Continue to transmision with the data in Transmit Buffer
			break;

		case SWITCH_TO_RX_MODE:
			UCB0IE |= UCRXIE;              // Enable RX interrupt
			UCB0IE &= ~UCTXIE;             // Disable TX interrupt
			UCB0CTLW0 &= ~UCTR;            // Switch to receiver
			MasterMode = RX_DATA_MODE;    // State state is to receive data
			UCB0CTLW0 |= UCTXSTT;          // Send repeated start
			if (RXByteCtr == 1)
			{
				//Must send stop since this is the N-1 byte
				while((UCB0CTLW0 & UCTXSTT));
				UCB0CTLW0 |= UCTXSTP;      // Send stop condition
			}
			break;

		case TX_DATA_MODE:
			if (TXByteCtr)
			{
				UCB0TXBUF = TransmitBuffer[TransmitIndex++];
				TXByteCtr--;
			}
			else
			{
				//Done with transmission
				UCB0CTLW0 |= UCTXSTP;     // Send stop condition
				MasterMode = IDLE_MODE;
				UCB0IE &= ~UCTXIE;                       // disable TX interrupt
				__bic_SR_register_on_exit(CPUOFF);      // Exit LPM0
			}
			break;

		default:
			__no_operation();
			break;
		}
		break;
		default: break;
	}
}


//******************************************************************************
// PORT2 Interrupt *************************************************************
//******************************************************************************
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
    P2IFG &= ~BIT5;                        		// Clear P2.5 IFG
	if(P2IN & GPIO_PIN5)						//if is high
	{
		P2IES |=  BIT5;        					// P2.5 Hi/Low edge
	}
	else										//if is low
	{
		P2IES &= ~BIT5;                        // P2.5 Low/Hi edge
		is_to_be_negotiated = 1;
	}
    CAPT_appStart();
}


//******************************************************************************
// TIMER1 Interrupt ***************************************************************
//******************************************************************************
/*
#pragma vector = TIMER1_A0_VECTOR
__interrupt void Timer1_A0_ISR(void)
{
	//ms
//	LED2_POUT ^= LED2_PIN;
	if(RisingEdge)
	{
		if(mSecTouch < MAXIMUM_SHORT_TOUCH_TIME + 1) mSecTouch ++;
	}

}*/

//******************************************************************************
// ADC1  Interrupt ***************************************************************
//******************************************************************************
/*
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=ADC_VECTOR
__interrupt void ADC_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(ADC_VECTOR))) ADC_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(ADCIV,ADCIV_ADCIFG))
    {
        case ADCIV_NONE:
            break;
        case ADCIV_ADCOVIFG:
            break;
        case ADCIV_ADCTOVIFG:
            break;
        case ADCIV_ADCHIIFG:
            break;
        case ADCIV_ADCLOIFG:
            break;
        case ADCIV_ADCINIFG:
            break;
        case ADCIV_ADCIFG:
            ADC_Result = ADCMEM0;
            break;
        default:
            break;
    }
}*/


// ADC interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=ADC_VECTOR
__interrupt void ADC_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(ADC_VECTOR))) ADC_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(ADCIV,ADCIV_ADCIFG))
    {
        case ADCIV_NONE:
            break;
        case ADCIV_ADCOVIFG:
            break;
        case ADCIV_ADCTOVIFG:
            break;
        case ADCIV_ADCHIIFG:
            break;
        case ADCIV_ADCLOIFG:
            break;
        case ADCIV_ADCINIFG:
            break;
        case ADCIV_ADCIFG:
            ADC_Result = ADCMEM0;
            __bic_SR_register_on_exit(LPM0_bits);            // Clear CPUOFF bit from LPM0
            break;
        default:
            break;
    }
}

/* I2C Write and Read Functions */

/* For slave device with dev_addr, writes the data specified in *reg_data
 *
 * dev_addr: The slave device address.
 *           Example: SLAVE_ADDR
 * reg_addr: The register or command to send to the slave.
 *           Example: CMD_TYPE_0_MASTER
 * *reg_data: The buffer to write
 *           Example: MasterType0
 * count: The length of *reg_data
 *           Example: TYPE_0_LENGTH
 *  */

/* For slave device with dev_addr, read the data specified in slaves reg_addr.
 * The received data is available in ReceiveBuffer
 *
 * dev_addr: The slave device address.
 *           Example: SLAVE_ADDR
 * reg_addr: The register or command to send to the slave.
 *           Example: CMD_TYPE_0_SLAVE
 * count: The length of data to read
 *           Example: TYPE_0_LENGTH
 *  */
I2C_Mode I2C_Master_ReadReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t count)
{
    /* Initialize state machine */
    MasterMode = TX_REG_ADDRESS_MODE;
    TransmitRegAddr = reg_addr;
    RXByteCtr = count;
    TXByteCtr = 0;
    ReceiveIndex = 0;
    TransmitIndex = 0;

    /* Initialize slave address and interrupts */
    UCB0I2CSA = dev_addr;
    UCB0IFG &= ~(UCTXIFG + UCRXIFG);       // Clear any pending interrupts
    UCB0IE &= ~UCRXIE;                       // Disable RX interrupt
    UCB0IE |= UCTXIE;                        // Enable TX interrupt

    UCB0CTLW0 |= UCTR + UCTXSTT;             // I2C TX, start condition
    __bis_SR_register(LPM0_bits + GIE);              // Enter LPM0 w/ interrupts
//  __bis_SR_register(GIE);              // Enter LPM0 w/ interrupts

    return MasterMode;

}


I2C_Mode I2C_Master_WriteReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t count)
{
    /* Initialize state machine */
    MasterMode = TX_REG_ADDRESS_MODE;
    TransmitRegAddr = reg_addr;

    //Copy register data to TransmitBuffer
    CopyArray(reg_data, TransmitBuffer, count);

    TXByteCtr = count;
    RXByteCtr = 0;
    ReceiveIndex = 0;
    TransmitIndex = 0;

    /* Initialize slave address and interrupts */
    UCB0I2CSA = dev_addr;
    UCB0IFG &= ~(UCTXIFG + UCRXIFG);       // Clear any pending interrupts
    UCB0IE &= ~UCRXIE;                       // Disable RX interrupt
    UCB0IE |= UCTXIE;                        // Enable TX interrupt

    UCB0CTLW0 |= UCTR + UCTXSTT;             // I2C TX, start condition
//    __bis_SR_register(LPM0_bits + GIE);              // Enter LPM0 w/ interrupts
    __bis_SR_register(GIE);              // Enter LPM0 w/ interrupts

    return MasterMode;
}

void CopyArray(uint8_t *source, uint8_t *dest, uint8_t count)
{
    uint8_t copyIndex = 0;
    for (copyIndex = 0; copyIndex < count; copyIndex++)
    {
        dest[copyIndex] = source[copyIndex];
    }
    __no_operation();
}

void initI2C (void)
{
    UCB0CTLW0 = UCSWRST;                                                    // Enable SW reset
    UCB0CTLW0 |= UCMODE_3 | UCMST | UCSSEL__SMCLK | UCSYNC;                 // I2C master mode, SMCLK
    UCB0BRW = 40;                                                           // fSCL = SMCLK/20 = ~100kHz
    UCB0I2CSA = I2C_SLAVE_ADDR;                                                       // Slave Address
    UCB0CTLW0 &= ~UCSWRST;                                                  // Clear SW reset, resume operation
    UCB0IE |= UCNACKIE;
}

/*
 * Power Delivery Output
 */
void set_pdo(uint16_t volt, uint16_t amp, uint8_t pdo_number)
{
    uint8_t buffer_tx[5] = {0};
    volatile uint32_t pdo_reg = 0;
    pdo_reg = (volt * 20);
    pdo_reg = pdo_reg << 10;
    pdo_reg += (amp * 100);

    buffer_tx[0] = pdo_reg & 0xFF;
    buffer_tx[1] = (pdo_reg & 0xFF00) >> 8;
    buffer_tx[2] = (pdo_reg & 0xFF0000) >> 16;
    buffer_tx[3] = (pdo_reg & 0xFF000000) >> 24;

    if(pdo_number == 2) I2C_Master_WriteReg(I2C_SLAVE_ADDR, 0x89, buffer_tx, 4);                          //Address PDO2
    if(pdo_number == 3) I2C_Master_WriteReg(I2C_SLAVE_ADDR, 0x8D, buffer_tx, 4);                          //Address PDO3
}

void change_pdo(uint8_t pdo_number)
{
    uint8_t buffer_tx[2] = {0};

    buffer_tx[0] = pdo_number;
    I2C_Master_WriteReg(I2C_SLAVE_ADDR, 0x70, buffer_tx, 1);                  //PDO select

    buffer_tx[0] = 0x0D;
    I2C_Master_WriteReg(I2C_SLAVE_ADDR, 0x51, buffer_tx, 1);                  //Soft Reset

    buffer_tx[0] = 0x26;
    I2C_Master_WriteReg(I2C_SLAVE_ADDR, 0x1A, buffer_tx, 1);                  //Send Command
}

void Check_Negoziatore (void)
{
	if(!(ATTACHED))
	{
		if (NEGOZIATORE == 15)
		{
			__delay_cycles(_500ms_Delay_C); //lasciare pausa di 500ms
			// Lasciare 2 ampere, cliente usa negoziatore da 30w
				//LED2_ON;
			__delay_cycles(_1ms_Delay_C);
			set_pdo(15,2,2);
				//LED2_OFF;
			__delay_cycles(_1ms_Delay_C);
			set_pdo(15,2,3);
				//LED2_ON;
			__delay_cycles(_1ms_Delay_C);
				change_pdo(2);
				//LED2_OFF;
		}
	}
}
void init_adc1 (void)
{
    SYSCFG2 |= ADCPCTL4;
    ADCCTL0 |= ADCSHT_2 | ADCON;                             // ADCON, S&H=16 ADC clks
    ADCCTL1 |= ADCSHP;                                       // ADCCLK = MODOSC; sampling timer
    ADCCTL2 |= ADCRES;                                       // 10-bit conversion results
    ADCMCTL0 |= ADCINCH_4;                                   // A1 ADC input select; Vref=AVCC
    ADCIE |= ADCIE0;                                         // Enable ADC conv complete interrupt
}

/*
 * Flash
 */

void FRAMWrite (void)
{

    FRAM_write_ptr = (unsigned long *)FRAM_TEST_START;
    SYSCFG0 = FRWPPW | PFWP;

    *FRAM_write_ptr = Lamp_State;
    *FRAM_write_ptr++;
    *FRAM_write_ptr = DimmIndex;
    *FRAM_write_ptr++;
    *FRAM_write_ptr = DimmDirection;

    SYSCFG0 = FRWPPW | PFWP | DFWP;
}

void FRAMRead (void)
{

    FRAM_Read_ptr = (unsigned long *)FRAM_TEST_START;
    SYSCFG0 = FRWPPW | PFWP;

    Lamp_State = (Lamp_State_typedef) *FRAM_Read_ptr;
    *FRAM_Read_ptr++;
    DimmIndex = *FRAM_Read_ptr;
    *FRAM_Read_ptr++;
    DimmDirection = (DimmDirection_typedef) *FRAM_Read_ptr;

    SYSCFG0 = FRWPPW | PFWP | DFWP;
    if (DimmIndex >= STEP_PWM_ARRAY_SIZE)
    {
        DimmIndex = 0;
        FRAMWrite();
    }
}
