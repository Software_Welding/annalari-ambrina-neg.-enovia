/*
 * State_Machine.h
 *
 *  Created on: 7 apr 2021
 *      Author: lucad
 */

#ifndef STATE_MACHINE_H_
#define STATE_MACHINE_H_

#define IS_TOUCHED CAPT_appHandler() == true
#define RESET_TOUCH Timer_State = false; mSecTouch = 0;
#define TIMER_PWM_OUT TA0CCR1

typedef enum Led_State_enum
{
    L_ON,
    L_OFF
} Led_State_typedef;
Led_State_typedef Led_State = L_OFF;

typedef enum Key_State_enum
{
    NOT_PRESSED,
    RISING_PRESS,
    SHORT_PRESS,
    LONG_PRESS,
    FALLING_SHORT_PRESS,
    FALLING_LONG_PRESS,
    DISABLED
} Key_State_typedef;
Key_State_typedef Key_State = NOT_PRESSED;
Key_State_typedef Old_Key_State = NOT_PRESSED;

uint16_t DimmLevel[STEP_PWM_ARRAY_SIZE];
int DimmIndex;
typedef enum DimmDirection_enum
{
    RISING,
    FALLING
} DimmDirection_typedef;
DimmDirection_typedef DimmDirection = RISING;

typedef enum Battery_State_enum
{
    BT_NORMAL,
    BT_FULL_CHARGE,
    BT_LOW,
    BT_VERY_LOW
} Battery_State_typedef;
Battery_State_typedef Battery_State = BT_NORMAL;

typedef enum Lamp_State_enum
{
    LAMP_OFF,
    LAMP_ON,
    LAMP_SHORT_PRESS,
    LAMP_LONG_PRESS,
    LAMP_DIMMERING,
    LAMP_NEED_CHARGE,
    LAMP_WAIT_NOT_PRESS,
	LAMP_FULL_CHARGE
} Lamp_State_typedef;
Lamp_State_typedef Lamp_State = LAMP_OFF;

bool Timer_State = false;
bool Timer_State_In_Charge = false;
uint16_t mSecTouch = 0;
uint16_t mSecADC = 0;
uint32_t time_elapse = 0;
uint16_t mSecDisableMachineStateTouch = 0;
uint16_t mSecBatteryInCharge = 0;
uint16_t stdTimer = 0;

uint16_t Key_Release_Counter = 1;
uint16_t Key_Pressed_Counter = 1;
uint16_t Key_Last_Release = 0;

uint8_t Array_Discharge_Timer_Index = 0;
#ifndef FAST_DEBUG
uint32_t Array_Discharge_Timer_Value[15] =
{
 1000,
 300000,
 240000, // 15 m
 180000,
 120000, // 20 m
 60000,
 60000,
 60000,
 60000,
 60000, // 25 m
 60000,
 60000,
 60000,
 60000,
 60000,
};
#else



uint32_t Array_Discharge_Timer_Value[15] =
{
 1000,
 30000,
 24000, // 15 m
 18000,
 12000, // 20 m
 6000,
 6000,
 6000,
 6000,
 6000, // 25 m
 6000,
 6000,
 6000,
 6000,
 6000,
};
#endif

uint32_t timer_discharge = 0;
uint32_t timer_charge = 0;

bool lamp_is_not_touched_in_charge_state = false;
bool lamp_in_discharge = false;
bool lamp_is_full_of_charge = false;
bool old_state_is_low_battery = false;
bool first_enter_low_battery = false;

#define OLD_STATE_IS_LOW_BATT old_state_is_low_battery == true
#define LAMP_IS_IN_DISCHARGE lamp_in_discharge == true
#define LAMP_CHARGE_IS_FULL lamp_is_full_of_charge == true
#define LAMP_NOT_TOUCHED_IN_DISCHARGE_MODE lamp_is_touched_in_discharge_state == false
#define NOT_TOUCHED_FROM_LOW_BATTERY lamp_is_not_touched_in_charge_state == true

bool Machine_State_Touch_Enable = true;

// Serve per collaudi negoziatore attaccato
bool Negoziatore_Attaccato = false;
bool Negoziatore_Attaccato_Prima = false;
#endif /* STATE_MACHINE_H_ */
