/*
 * setup.h
 *
 *  Created on: 7 apr 2021
 *      Author: lucad
 */
#ifndef SETUP_H_
#define SETUP_H_

/*
 * Negoziatore
 */
#define NEGOZIATORE_ESTERNO
//#define FAST_DEBUG
/*
 * ADC Lettura batteria per inizio VRef = 3.3v
 */
#define BATTERY_IN_DISCHARGE                   575  // 10.1 vbatt
#define BATTERY_FULL						   688  //693 12.1v
//#define BATTERY_FULL                           709  // 12.5 vbatt
#define STARTUP_THREESHOLD                     560  // 10.1 vbatt
#define THREESHOLD_OUT_LOW_BATTERY              50  // 10.9 vbatt

/*
 * Touch
 */
#define MINIMUM_SHORT_TOUCH_TIME                0
#define MAXIMUM_SHORT_TOUCH_TIME               400

/*
 * Rampe delay
 */
#define MANUAL_RAMP_DELAY_TIME               10000      //Cycle
#define AUTOMATIC_RAMP_DELAY_TIME             5000      //Cycle
#define HALF_SEC_RAMP_DELAY_TIME           8000000      //Cycle
#define _250ms_Delay_C			    (16000000 / 4)		//Cycle
#define _50ms_Delay_C		  (_250ms_Delay_C / 5)
#define _1ms_Delay_C          (_50ms_Delay_C / 50)
#define _500ms_Delay_C			    (16000000 / 2)
/*
 * PWM Duty cycle
 */
#define DUTY_OFF 0
#define MAX_DUTY 199


/*
 * Tempi usati dai timer
 */
#define _5_Sec 5000
#define _1_Sec 1000

#ifndef FAST_DEBUG
#define _15_Min 900000
#else
#define _15_Min 90000
#endif

#define _10_Min 600000
#define _1_Min 60000
//#define _15_Min 35000
//#define _1_Min 6000

/*
 * Indirizzi I2C stbusb4500
 */
#define I2C_SLAVE_ADDR 0x28

/*
 * Variabili globali
 */
char is_to_be_negotiated = 0;
unsigned long i = 0;
unsigned long ADC_Result;

#define BATTERIA_SI_STA_SCARICANDO ((ADC_Result < BATTERY_IN_DISCHARGE) || (first_enter_low_battery == true))
#define BATTERIA_CARICA ADC_Result > BATTERY_FULL

//******************************************************************************
// Configuratore Lampade     ***************************************************
//******************************************************************************

/*
 * Lampada: Ambrina
 */
#define AMBRINA
#ifdef AMBRINA
    // PWM
    #define MIN_PWM 5
    #define MAX_PWM MAX_DUTY

    // Sweep
    #define SWEEP_ENABLE
    #ifdef SWEEP_ENABLE
        //#define SWEEP_ENABLE_DIMMERING
        #define SWEEP_TIME_DIMMERING 30000
        #define SWEEP_ENABLE_ON_OFF
        #define SWEEP_TIME_ON_OFF 10000
    #endif

    // Ricarica batteria + negoziatore
    #define NEGOZIATORE 15
	// Alto = Staccata - Basso = attaccata
    //#define ATTACHED P2IN & GPIO_PIN3
	#define ATTACHED (!(P2IN & GPIO_PIN3))

    // Step PWM
    #define STEP_PWM
        #ifdef STEP_PWM
        #define STEP_PWM_ARRAY_SIZE 4
    #endif
    #define STATE_MACHINE_FUNCTION Lamp_State_Machine_STEPS();
#endif

/*
 * Lampada: Fiammetta
 * Ancora da configurare il firmware
 * con le funzioni + macchina degli stati
 */
//#define FIAMMETTA
#ifdef FIAMMETTA
    // PWM
    #define MIN_PWM 10
    #define MAX_PWM 180

    // Sweep
    #define SWEEP_ENABLE
    #ifdef SWEEP_ENABLE
        //#define SWEEP_ENABLE_DIMMERING
        #define SWEEP_TIME_DIMMERING 30000
        #define SWEEP_ENABLE_ON_OFF
        #define SWEEP_TIME_ON_OFF 10000
    #endif

    // Ricarica batteria + negoziatore
    #define NEGOZIATORE 9
    #define SP2IN & GPIO_PIN5

    // Step PWM
    //#define STEP_PWM
    #ifdef STEP_PWM
        #define STEP_PWM_ARRAY_SIZE 4
    #endif
    #define STATE_MACHINE_FUNCTION Lamp_State_Machine_LINEAR();
#endif



//******************************************************************************
// General I2C State Machine ***************************************************
//******************************************************************************

typedef enum I2C_ModeEnum{
    IDLE_MODE,
    NACK_MODE,
    TX_REG_ADDRESS_MODE,
    RX_REG_ADDRESS_MODE,
    TX_DATA_MODE,
    RX_DATA_MODE,
    SWITCH_TO_RX_MODE,
    SWITHC_TO_TX_MODE,
    TIMEOUT_MODE
} I2C_Mode;


/* Used to track the state of the software state machine*/
I2C_Mode MasterMode = IDLE_MODE;

/* The Register Address/Command to use*/
uint8_t TransmitRegAddr = 0;

/* ReceiveBuffer: Buffer used to receive data in the ISR
 * RXByteCtr: Number of bytes left to receive
 * ReceiveIndex: The index of the next byte to be received in ReceiveBuffer
 * TransmitBuffer: Buffer used to transmit data in the ISR
 * TXByteCtr: Number of bytes left to transfer
 * TransmitIndex: The index of the next byte to be transmitted in TransmitBuffer
 * */

#define MAX_BUFFER_SIZE     20

uint8_t ReceiveBuffer[MAX_BUFFER_SIZE] = {0};
uint8_t RXByteCtr = 0;
uint8_t ReceiveIndex = 0;
uint8_t TransmitBuffer[MAX_BUFFER_SIZE] = {0};
uint8_t TXByteCtr = 0;
uint8_t TransmitIndex = 0;



#endif /* SETUP_H_ */
